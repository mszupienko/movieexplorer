import React from 'react';
import './SearchBox.css';

export default class SearchBox extends React.Component {
    render () {
        return (
            <div className='col-12'>
                <div className='row d-flex justify-content-center'>
                        <input type='search' className='form-control mt-5 searchbox--input' placeholder='Search movie title...'/>
                </div>
            </div>
        );
    }
}