import React from 'react';
import './MovieTableItem.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class MovieTableItem extends React.Component {
    render () {
        const { id, image, title, rate, votes } = this.props;
        return (
            <tr>
                <td>{ id }</td>
                <td><img className='movie--table__image' src={`https://image.tmdb.org/t/p/w185/${ image }`} alt={ title } /></td>
                <td>{ title }</td>
                <td><FontAwesomeIcon icon={['fas', 'star']} color="#c0b283" /> { rate } </td>
                <td>{ votes } <FontAwesomeIcon icon={['fas', 'user-check']} color="#c0b283" /></td>
            </tr>  
        );
    }
}
