import React from 'react';
import './Header.css';
import SearchBox from '../SearchBox/SearchBox';
import camera from './video-camera.png';

export default class Header extends React.Component {
    render () {
        return (
            <div className='col-12'>
                <div className='row'>
                    <div className='header--title'>
                        <h1 className='mt-5'>M<img  className="header--img" src={camera} />vie Explorer</h1>
                        <SearchBox />
                    </div>
                </div>
            </div>
        );
        
    }
}