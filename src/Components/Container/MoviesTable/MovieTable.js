import React from 'react';
import './MovieTable.css';
import MovieTableItem from '../../Presentational/MovieTableItem/MovieTableItem';
import axios from 'axios';
import { API_URL, API_KEY } from '../../../TheMovieDb_API';

export default class MovieTable extends React.Component {
    constructor() {
        super();
        this.state = {
            top_rated: []
        };
    }
    componentDidMount () {
        axios.get(`${ API_URL }/movie/top_rated?api_key=${ API_KEY }&language=en-US&page=1`).then((response) => {
            const top_rated = response.data.results.map((movie) => {
                return {
                    id: movie.id,
                    image: movie.poster_path,
                    title: movie.title,
                    rate: movie.vote_average,
                    votes: movie.vote_count
                }
            });
            this.setState({
                top_rated
            });
        });

    };

    render() {
        const { top_rated } = this.state;
        return (
            <div className='col-12'>
                <div className='movies mt-5 justify-content-center'>
                    <table className='movies--table'>
                        <thead>
                            <tr>
                                <th>Pos.</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Rate</th>
                                <th>Votes</th>
                            </tr>
                        </thead>
                        <tbody>
                            { top_rated.map((movie, key) => {
                                const id = key + 1;
                                const { image, title, rate, votes } = movie;
                                return (
                                    <MovieTableItem 
                                        key = { key }
                                        id={ id }
                                        image={ image }
                                        title={ title }
                                        rate={ rate }
                                        votes={ votes } />);
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
