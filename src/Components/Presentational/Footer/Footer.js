import React from 'react';
import './Footer.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class MovieTableItem extends React.Component {
    render () {
        return (
            <div className="row footer">
                <div className="col-12">
                    <div className="d-flex justify-content-around mt-2">
                        <span className="footer--creator">Project created by: Marek Szupieńko</span>
                        <div className="col-3 d-flex justify-content-around">
                            <div className="col-1">
                            <a href="https://bitbucket.org/mszupienko/movieexplorer/src/master/"><FontAwesomeIcon icon={['fab', 'bitbucket']} color="#c0b283" size="2x"/></a>
                            </div>
                            <div className="col-1">
                            <a href="https://www.linkedin.com/in/marek-szupieńko"><FontAwesomeIcon icon={['fab', 'linkedin']} color="#c0b283" size="2x"/></a>
                            </div>
                            <div className="col-1">
                            <a href="mailto:marek.szupienko@gmail.com"><FontAwesomeIcon icon={['fa', 'envelope']} color="#c0b283" size="2x"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}