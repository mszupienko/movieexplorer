import React, { Component } from 'react';
import './App.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import Header from './Components/Container/Header/Header';
import MovieTable from './Components/Container/MoviesTable/MovieTable';
import Footer from './Components/Presentational/Footer/Footer';
library.add(fab, fas, faEnvelope);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <MovieTable />
        <Footer />
      </div>
    );
  }
}

export default App;
